<?php 

$itemscope = get_post_meta(get_the_ID(), '_establishment_details_itemscope', true);
$itemscope = ($itemscope!='') ? $itemscope : 'LocalBusiness';

?>

<article itemscope itemtype="http://schema.org/<?php echo $itemscope; ?>" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 itemprop="name" class="entry-title"><strong>', '</strong></h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	</div><!-- .entry-header -->
	<div class="entry-content">
		<?php 
		
		/* Details */
		
		$address = get_post_meta(get_the_ID(), '_establishment_details_address', true); 
		$address_city = get_post_meta(get_the_ID(), '_establishment_details_address_city', true);
		$address_region = get_post_meta(get_the_ID(), '_establishment_details_address_region', true);
		$address_postal = get_post_meta(get_the_ID(), '_establishment_details_address_postal', true);
		if( $address != '' ) {
			echo "<p itemprop=\"address\" itemscope itemtype=\"http://schema.org/PostalAddress\">
			<strong>Address</strong>: <span itemprop=\"streetAddress\">{$address}</span>,
			<span itemprop=\"addressLocality\">{$address_city}</span>,
			<span itemprop=\"addressRegion\">{$address_region}</span>,
			<span itemprop=\"addressCountry\">Philippines</span>,
			<span itemprop=\"postalCode\">{$address_postal}</span>
			</p>";
		}
		
		$phone = get_post_meta(get_the_ID(), '_establishment_details_phone', true);
		if( $phone != '' ) {
			echo "<p><strong>Telephone Number(s)</strong>: <span itemprop=\"telephone\">{$phone}</span></p>";
		} 
		$mobile = get_post_meta(get_the_ID(), '_establishment_details_mobile', true); 
		if( $mobile != '' ) {
			echo "<p><strong>Mobile Number(s)</strong>: {$mobile}</p>";
		}
		$fax = get_post_meta(get_the_ID(), '_establishment_details_fax', true); 
		if( $fax != '' ) {
			echo "<p><strong>Fax Number(s)</strong>: {$fax}</p>";
		}
		$website = get_post_meta(get_the_ID(), '_establishment_details_website', true); 
		if( $website != '' ) {
			echo "<p><strong>Website</strong>: <a href=\"{$website}\" itemprop=\"url\" target=\"_blank\" rel=\"nofollow\">{$website}</a></p>";
		}
		
		?>

<?php 
				
				/* Social Media */
		
		$facebook = get_post_meta(get_the_ID(), '_establishment_social_media_facebook', true); 
		$google = get_post_meta(get_the_ID(), '_establishment_social_media_google', true);
		$twitter = get_post_meta(get_the_ID(), '_establishment_social_media_twitter', true);
		$linkedin = get_post_meta(get_the_ID(), '_establishment_social_media_linkedin', true);
		$youtube = get_post_meta(get_the_ID(), '_establishment_social_media_youtube', true);
		$vimeo = get_post_meta(get_the_ID(), '_establishment_social_media_vimeo', true);
		$blogger = get_post_meta(get_the_ID(), '_establishment_social_media_blogger', true);
		$behance = get_post_meta(get_the_ID(), '_establishment_social_media_behance', true);
		$deviantart = get_post_meta(get_the_ID(), '_establishment_social_media_deviantart', true);
		$digg = get_post_meta(get_the_ID(), '_establishment_social_media_digg', true);
		$dribble = get_post_meta(get_the_ID(), '_establishment_social_media_dribble', true);
		$pinterest = get_post_meta(get_the_ID(), '_establishment_social_media_pinterest', true);
		$stumbleupon = get_post_meta(get_the_ID(), '_establishment_social_media_stumbleupon', true);
		$tumblr = get_post_meta(get_the_ID(), '_establishment_social_media_tumblr', true);
		
		$social_media = array();
		if( $facebook != '' ) {
			$social_media[] = "<li><a href='{$facebook}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/facebook.png'></a></li>";
		}
		if( $google != '' ) {
			$social_media[] = "<li><a href='{$google}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/google.png'></a></li>";
		}
		if( $twitter != '' ) {
			$social_media[] = "<li><a href='{$twitter}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/twitter.png'></a></li>";
		}
		if( $linkedin != '' ) {
			$social_media[] = "<li><a href='{$linkedin}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/linkedin.png'></a></li>";
		}
		if( $youtube != '' ) {
			$social_media[] = "<li><a href='{$youtube}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/youtube.png'></a></li>";
		}
		if( $vimeo != '' ) {
			$social_media[] = "<li><a href='{$vimeo}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/vimeo.png'></a></li>";
		}
		if( $blogger != '' ) {
			$social_media[] = "<li><a href='{$blogger}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/blogger.png'></a></li>";
		}
		if( $behance != '' ) {
			$social_media[] = "<li><a href='{$behance}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/behance.png'></a></li>";
		}
		if( $deviantart != '' ) {
			$social_media[] = "<li><a href='{$deviantart}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/deviantart.png'></a></li>";
		}
		if( $digg != '' ) {
			$social_media[] = "<li><a href='{$digg}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/digg.png'></a></li>";
		}
		if( $dribble != '' ) {
			$social_media[] = "<li><a href='{$dribble}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/dribble.png'></a></li>";
		}
		
		if( $pinterest != '' ) {
			$social_media[] = "<li><a href='{$pinterest}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/pinterest.png'></a></li>";
		}
		if( $stumbleupon != '' ) {
			$social_media[] = "<li><a href='{$stumbleupon}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/stumbleupon.png'></a></li>";
		}
		if( $tumblr != '' ) {
			$social_media[] = "<li><a href='{$tumblr}' target='_blank'><img src='".get_stylesheet_directory_uri()."/img/social/tumblr.png'></a></li>";
		}
		
		if( count( $social_media ) > 0 ) {
			
		echo '<div class="site-main new"><div class="hentry"><div class="entry-content">';
		echo '<ul class="social-media">';
			echo implode('', $social_media);
		echo '</ul>';
		echo '</div></div></div>';
		
		}
			
	// Gallery
	if ( function_exists( 'envira_gallery' ) ) { 
		$gallery = envira_gallery( get_the_ID(), 'id', array(), true);
		if( $gallery ) {
			echo '<div class="site-main new"><div class="hentry"><div class="entry-content">';
				echo $gallery;
			echo '</div></div></div>';
		}
	}

?>	
	</div><!-- .entry-content -->
</article>
