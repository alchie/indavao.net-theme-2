<?php

require('inc/bootstrap3-menu-walker.php');
require('inc/frontend_ajax.php');

if ( ! function_exists( 'indavaonet_setup' ) ) :

function indavaonet_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on indavaonet, use a find and replace
	 * to change 'indavaonet' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'indavaonet', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'indavaonet' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	/* add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) ); */

}
endif; // indavaonet_setup
add_action( 'after_setup_theme', 'indavaonet_setup' );

function indavaonet_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'indavaonet' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'indavaonet' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s panel panel-default"><div class="panel-heading">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h3 class="widget-title panel-title">',
		'after_title'   => '</h3></div><div class="panel-body">',
	) );
}
add_action( 'widgets_init', 'indavaonet_widgets_init' );

function indavaonet_scripts() {

	wp_enqueue_style( 'OpenSans-font', '//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800');
	wp_enqueue_style( 'Merriweather-font', '//fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic');
	
	// Add Bootstrap, used in the main stylesheet.
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.1.0' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20150210' );
	//wp_enqueue_style( 'bootstrap-cdn-css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), '3.3.2' );
	//wp_enqueue_script( 'bootstrap-cdn-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array(), '20150210' );
	
	// font awesome
	wp_enqueue_style( 'font-awesome.min.css', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css', array(), '3.1.0' );
	
	//wp_enqueue_script( 'google-maps-api', '//maps.googleapis.com/maps/api/js?key=AIzaSyCsz95GSXELp54QaQ-KqtcfSjcniNmTFT4', array(), '3.0' );
	wp_enqueue_script( 'google-maps-api', '//maps.googleapis.com/maps/api/js?v=3.exp', array(), '3.0' );
	
	// WOW  Animation
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/css/animate.min.css', array(), '1.0' );
	
	wp_enqueue_script( 'jquery.easing.js', get_template_directory_uri() . '/js/jquery.easing.min.js', array('jquery'), '1.0.3', true );
	wp_enqueue_script( 'jquery.fittext.js', get_template_directory_uri() . '/js/jquery.fittext.js', array('jquery'), '1.0.3', true );
	wp_enqueue_script( 'wow-min-js', get_template_directory_uri() . '/js/wow.min.js', array('jquery', 'jquery.easing.js', 'jquery.fittext.js'), '1.0.3', true );
	
	// Load our main stylesheet.
	wp_enqueue_style( 'creative-css', get_template_directory_uri() . '/css/creative.css', array('animate-css', 'bootstrap-css'), '1.0' );
	wp_enqueue_script( 'creative-js', get_template_directory_uri() . '/js/creative.js', array('jquery', 'bootstrap-js', 'wow-min-js'), '1.0.0', true );
	//wp_enqueue_style( 'indavaonet', get_stylesheet_uri(), array(), '20150507.5' );

	// Load the Internet Explorer specific stylesheet.
	//wp_enqueue_style( 'indavaonet-ie', get_template_directory_uri() . '/css/ie.css', array( 'indavaonet-style' ), '20141010' );
	//wp_style_add_data( 'indavaonet-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	//wp_enqueue_style( 'indavaonet-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'indavaonet-style' ), '20141010' );
	//wp_style_add_data( 'indavaonet-ie7', 'conditional', 'lt IE 8' );

	//if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		//wp_enqueue_script( 'comment-reply' );
	//}

	//wp_enqueue_script( 'indavaonet-script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery', 'bootstrap-js' ), '20150506.2', true );

}
add_action( 'wp_enqueue_scripts', 'indavaonet_scripts' );


if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
	function jetpackme_remove_rp() {
		$jprp = Jetpack_RelatedPosts::init();
		$callback = array( $jprp, 'filter_add_target_to_dom' );
		remove_filter( 'the_content', $callback, 40 );
	}
	add_filter( 'wp', 'jetpackme_remove_rp', 20 );
}


add_action('wp_footer', 'wp_footer_frontpage');
function wp_footer_frontpage() {
	if( is_front_page() ) {
?>
<script>
(function($){
	var dropMarker = function( map, data, timeout ) {
	
	var mapgeo = data.mapgeo;
	var link = data.link;
	var title = data.title;
	var latlng = mapgeo.split(',');
							
	window.setTimeout(function() {
	
	var infowindow = new google.maps.InfoWindow({
		content: "<span style='color:#000;font-weight:bold'>"+title+"</span>"
	});

	  var marker = new google.maps.Marker({
			  position: new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])),
			  map: map,
			  icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADd0lEQVRoQ+Va7XXaMBSVGKDBE5ROUDJBnQlCByihE4ROkGSCkAkCdICQCeJOUHeC0glwOgDqFcjn+JDYus+S+dHonJ7+4El69933KUer/2Tp2Dj+js/OjTYjo9QAZ/fxb+juyPF/gQvX2ujVu+XTY8y7owDZTNJ+z6hbKD9yyjM6WlCrrVbfknlWMBuaZIKBPI/TK6PVVADgUJ9CGzU7WWY3IWBaA7EsQIGniuuE6GH35jDIWVt2WgEBiKEDYWMg5loDzGeAsfEkWmIgjonfAa7kU9CCOZUyIwZSXKQ/I7pTHai8v8hOfYirv4uAbMbptdbqSnJBW1lj1E2yzK7Z/TSQI7jUi2wGF/vAuhgPZJzOwcYFa6EYcmBlAVYmzFk0EMTGpsMAr9O1QKwk0YDArUZItw/MgaUMrHmnempeplKbstVWTaWsunS88t1NMYIgp90Kbcqz0iqtqwU7QEZluPjEp5z93RoE7mU7h8bFAslgyU++w3YX72tAY0FzBdWmce8CkB8AkvoEKSBs7WCtZ5USsEzVFBYIPMa/GDbKUySsIOC9enoF7MVghALCXFg1R8xz3xYQ+DMV7F24VtRgp4GQqVIS7ADyiKxlJ88o6ZduFhlWJIHONo9UjGy+pBPdU/c+q7jfCzfpvVpLpENZ3Mq+H2ttr0UvWGiOh4W7aouCB4pLpL8Jfci+wCZMB0wx4nw6R3X/KFEiVJYNdHuPBMgMQC5DlZPsN1s8FX3PZsweHsgkHcC97Kx+tOUGqzVzIQ3k2O4lcSuRa+2AyLIXY8haGbjVV7jVnD1ExgiyF2aJNTtLsEocytmZJllkojczERDnXp0HPVsEqwaQA+k46N2EOWBqRxAQxwo9+krdqw0b4mAvlUKb0UkqbstGayCOFbqRZFlpy0YYkMgZDCD+4PloKI2N0kjiYK9aF3Vliq74lrV4kxzb5dadEQQkVrWXVvHXwIQD2X/0od6o6qwp6ak6YyS0SIYEeHAdObSK/eSAd107r7yXxAtA/MI8Xn6+lmx9IRvsWpXakrrvirRCzHzPHhYNiLS2SIYmBkxUIGwWi5GlDsHFB4L2Ba1+Xtfqh7QhTcxEB7JjpeHDkHsqyhh3kch0AqQuJcdKtZ0UxCar4al1hZR8bmXYp08JC9HrSN3lrr7s3ai3+xwX/FdAnVb2tlaMue8f60elQkBEleoAAAAASUVORK5CYII=',
			  animation: google.maps.Animation.DROP,
			  url : link,
			  title : title
		  });
			google.maps.event.addListener(marker, 'click', function() {
				window.location.href = this.url;
			});
			google.maps.event.addListener(marker, "mouseover", function() {
				infowindow.open(map, this);
			});
			google.maps.event.addListener(marker, "mouseout", function() {
				infowindow.close(this);
			});
		}, timeout);
	}
	var getLocalBusinessMarkers = function(map){
			$.ajax({
					url : '/wp-admin/admin-ajax.php?action=indavao2',
					type: 'POST',
					data: {json : 'localbusiness'},
					dataType: 'json',
					success : function(data, textStatus, jqXHR)
					{
						
						if(typeof data.error === 'undefined')
						{
							for(var i in data) {
								dropMarker( map, data[i], i * 300 );
							}
						}
						else
						{
							// Handle errors here
							console.log('ERRORS: ' + data.error);
						}
					},
			});
	};
	var map_initialize_homepage = function() {
	  var map = new google.maps.Map(document.getElementById('map-canvas'), {
		zoom: 13,
		center: {
			lat: 7.0700517, lng: 125.5976151
			},
		zoomControl: true,
		zoomControlOptions: {
		  style: google.maps.ZoomControlStyle.SMALL,
		  position: google.maps.ControlPosition.RIGHT_BOTTOM
		},
		disableDefaultUI: true
	  });
	  
	  getLocalBusinessMarkers(map);
	  
	}
	 google.maps.event.addDomListener(window, 'load', map_initialize_homepage);
})(jQuery);
</script>
<?php
	}
}

add_action('wp_footer', 'wp_footer_single_establishment');
function wp_footer_single_establishment() {
	if( is_single() ) {
		$mapgeo = get_post_meta(get_the_ID(), '_establishment_details_mapgeo', true);
		$lat = '7.0700517';
		$lng = '125.5976151';
		if( $mapgeo != '') {
			$latlng = explode(',', $mapgeo);
			$lat = $latlng[0];
			$lng = $latlng[1];
		}
?>
<script>
(function($){
	var map_initialize_homepage = function() {
	  var map = new google.maps.Map(document.getElementById('map-canvas'), {
		zoom: 13,
		center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>},
		zoomControl: true,
		zoomControlOptions: {
		  style: google.maps.ZoomControlStyle.SMALL,
		  position: google.maps.ControlPosition.RIGHT_BOTTOM
		},
		disableDefaultUI: true,
		draggable: false,
	  });
	  
	  var marker = new google.maps.Marker({
		  position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
		  map: map,
		  icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADd0lEQVRoQ+Va7XXaMBSVGKDBE5ROUDJBnQlCByihE4ROkGSCkAkCdICQCeJOUHeC0glwOgDqFcjn+JDYus+S+dHonJ7+4El69933KUer/2Tp2Dj+js/OjTYjo9QAZ/fxb+juyPF/gQvX2ujVu+XTY8y7owDZTNJ+z6hbKD9yyjM6WlCrrVbfknlWMBuaZIKBPI/TK6PVVADgUJ9CGzU7WWY3IWBaA7EsQIGniuuE6GH35jDIWVt2WgEBiKEDYWMg5loDzGeAsfEkWmIgjonfAa7kU9CCOZUyIwZSXKQ/I7pTHai8v8hOfYirv4uAbMbptdbqSnJBW1lj1E2yzK7Z/TSQI7jUi2wGF/vAuhgPZJzOwcYFa6EYcmBlAVYmzFk0EMTGpsMAr9O1QKwk0YDArUZItw/MgaUMrHmnempeplKbstVWTaWsunS88t1NMYIgp90Kbcqz0iqtqwU7QEZluPjEp5z93RoE7mU7h8bFAslgyU++w3YX72tAY0FzBdWmce8CkB8AkvoEKSBs7WCtZ5USsEzVFBYIPMa/GDbKUySsIOC9enoF7MVghALCXFg1R8xz3xYQ+DMV7F24VtRgp4GQqVIS7ADyiKxlJ88o6ZduFhlWJIHONo9UjGy+pBPdU/c+q7jfCzfpvVpLpENZ3Mq+H2ttr0UvWGiOh4W7aouCB4pLpL8Jfci+wCZMB0wx4nw6R3X/KFEiVJYNdHuPBMgMQC5DlZPsN1s8FX3PZsweHsgkHcC97Kx+tOUGqzVzIQ3k2O4lcSuRa+2AyLIXY8haGbjVV7jVnD1ExgiyF2aJNTtLsEocytmZJllkojczERDnXp0HPVsEqwaQA+k46N2EOWBqRxAQxwo9+krdqw0b4mAvlUKb0UkqbstGayCOFbqRZFlpy0YYkMgZDCD+4PloKI2N0kjiYK9aF3Vliq74lrV4kxzb5dadEQQkVrWXVvHXwIQD2X/0od6o6qwp6ak6YyS0SIYEeHAdObSK/eSAd107r7yXxAtA/MI8Xn6+lmx9IRvsWpXakrrvirRCzHzPHhYNiLS2SIYmBkxUIGwWi5GlDsHFB4L2Ba1+Xtfqh7QhTcxEB7JjpeHDkHsqyhh3kch0AqQuJcdKtZ0UxCar4al1hZR8bmXYp08JC9HrSN3lrr7s3ai3+xwX/FdAnVb2tlaMue8f60elQkBEleoAAAAASUVORK5CYII=',
		  animation: google.maps.Animation.DROP,
		  title : '<?php the_title(); ?>'
	  });
		  
	}
	 google.maps.event.addDomListener(window, 'load', map_initialize_homepage);
})(jQuery);
</script>
<?php
	}
}

add_action( 'init', 'query_redirection_to_home' );
function query_redirection_to_home() {
	if( isset($_GET['p']) ||  isset($_GET['q']) ) {
		header('location: /');
		exit;
	}
}
