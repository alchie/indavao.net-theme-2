<?php

add_action( 'wp_ajax_indavao2', 'indavao2_callback' );
add_action( 'wp_ajax_nopriv_indavao2', 'indavao2_callback' );

function indavao2_callback() {
		
	if( isset($_REQUEST['json']) && ($_REQUEST['json'] != '') ) {
		switch( $_REQUEST['json'] ) {
			case 'localbusiness':
					indavao2_localbusiness();
			break;
			case 'establishment':
				if( isset($_REQUEST['id']) && ($_REQUEST['id'] != '') ) {
					indavao2_establishment( $_REQUEST['id'] );
				}
			break;
		}
	}
	wp_die();
}

function indavao2_localbusiness( $offset=0, $posts_per_page=99 ) {
	$response = array();
	$items = get_posts( array( 
		'post_type'=>'establishment',
		'posts_per_page'   => $posts_per_page,
		'offset' => $offset,
		'orderby' => 'post_title',
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key'     => '_establishment_details_mapgeo',
				'value'   => '',
				'compare' => '!=',
			),
		),
	));
	
	$n=0;
	foreach( $items as $item ) {
		$mapgeo = get_post_meta($item->ID, '_establishment_details_mapgeo', true);
		
		$response[] = array(
			//'id'=>$item->ID,
			'title'=>$item->post_title,
			'mapgeo'=> $mapgeo,
			'link'=>get_permalink($item->ID),
			);
		$n++;
	} 
	
	//$term = get_term( $id, 'establishment-category');
	
	echo json_encode( $response );
}

function indavao2_establishment( $id ) {
	$post = get_post( $id );
	if( ! $post ) { 
		echo json_encode( false );
		exit;
	}
	$street = get_post_meta($id, '_establishment_details_address', true);
	$address = implode(', ', array(
		$street,
		get_post_meta($id, '_establishment_details_address_city', true),
		get_post_meta($id, '_establishment_details_address_region', true),
		'Philippines',
		get_post_meta($id, '_establishment_details_address_postal', true)
	));
	$phone = get_post_meta($id, '_establishment_details_phone', true);
	$mobile = get_post_meta($id, '_establishment_details_mobile', true); 
	$fax = get_post_meta($id, '_establishment_details_fax', true); 
	$website = get_post_meta($id, '_establishment_details_website', true); 
	$mapgeo = get_post_meta($id, '_establishment_details_mapgeo', true); 
	$logo = wp_get_attachment_image_src( get_post_meta($item->ID, '_establishment_logo_logo', true), 'full' );
	$thumbnail = wp_get_attachment_url( get_post_thumbnail_id($id, 'thumbnail') );
	
	$response = array(
		// details
		'id' => $id,
		'title' => $post->post_title,
		'address' => $address,
		'telephone' => $phone,
		'mobile' => $mobile,
		'fax' => $fax,
		'website' => $website,
		'thumbnail' => (($thumbnail)?$thumbnail:''),
		'mapgeo' => $mapgeo,
		'logo' => (($logo)?$logo:''),
		'flickr_photoset' => get_post_meta($id, '_establishment_details_flickr_photoset', true),
		'flickr_userid' => get_post_meta($id, '_establishment_details_flickr_userid', true),
		
		// advertising
		'call_active' => get_post_meta($id, '_establishment_advertising_call_active', true),
		'email_active' => get_post_meta($id, '_establishment_advertising_email_active', true),
		'primary_phone' => get_post_meta($id, '_establishment_advertising_phone_primary', true),
		'primary_email' => get_post_meta($id, '_establishment_advertising_email_primary', true),
		'share_active' => get_post_meta($id, '_establishment_advertising_share_active', true),
		'reviews_active' => get_post_meta($id, '_establishment_advertising_reviews_active', true),
		'map_active' => get_post_meta($id, '_establishment_advertising_map_active', true),
		'gallery_active' => get_post_meta($id, '_establishment_advertising_gallery_active', true),
		'favorite_active' => get_post_meta($id, '_establishment_advertising_favorite_active', true),
		'verified' => get_post_meta($id, '_establishment_advertising_verified', true),
		'terms' => get_the_terms( $id, 'establishment-category')
	);
	
	echo json_encode( $response );
}
