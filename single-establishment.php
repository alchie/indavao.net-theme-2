<?php 

$itemscope = get_post_meta(get_the_ID(), '_establishment_details_itemscope', true);
$itemscope = ($itemscope!='') ? $itemscope : 'LocalBusiness';

get_header(); ?>

	<header>
        <div id="map-canvas"></div>
    </header>
	
	<div id="establishment">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
					
					<div class="content-area text-center" id="establishment-area">
						<!--<div class="vertical-center"><img src="<?php echo get_template_directory_uri(); ?>/img/ajax-loader.gif"></div>-->
							
<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'content', 'establishment' );
		endwhile;
?>

					</div>										
					
                </div>
            </div>
        </div>
    </div>
    
<?php get_footer(); ?>
