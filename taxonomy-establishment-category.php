<?php get_header(); ?>
	<div id="list">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
					
		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); 
			
			get_template_part( 'content', 'establishment' );
			?>


					
						
														

<?php 
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );


		 endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
